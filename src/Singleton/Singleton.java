package Singleton;

import tinyjdk.Myclass;

public class Singleton {
    private static Singleton instance = null;
    private int x;

    private Singleton(int x) {
        this.x = x;
    }

    public int getField() { return x; }


    public static Singleton getInstance(int x){
        if(instance == null || instance.x != x)
            instance = new Singleton(x);
        return instance;
    }
}
