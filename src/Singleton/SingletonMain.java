package Singleton;

public class SingletonMain {
    public static void main(String[] args){
        Singleton s = Singleton.getInstance(5);
        Singleton s1 = Singleton.getInstance(5);

        Singleton s2 = Singleton.getInstance(8);

        System.out.println(s.equals(s2));

        System.out.println(s.equals(s1));
    }
}
