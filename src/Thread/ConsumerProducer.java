package Thread;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ConsumerProducer {
    private static BlockingQueue<Integer> l = new LinkedBlockingQueue<>();

    public static class Consumer extends Thread {
        @Override
        public void run() {
            for(int i= 0; i<150000; i++){
                try {
                    int n = l.take();
                    System.out.println(n);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Producer extends Thread{
        @Override
        public void run() {
            for(int i = 0; i<150000;i++){

                l.add(i);


            }

        }

    }
    public static void main(String[] args) throws InterruptedException {
        try {
            Consumer c = new Consumer();
            Producer p = new Producer();

            p.start();
            c.start();

            p.join();
            c.join();
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }

        System.out.println("finito");
    }
}
