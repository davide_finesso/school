package Thread;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool {
    private List<PooledThread> l = new ArrayList<>();



    private class PooledThread extends Thread{
        private Runnable r;

        public PooledThread(Runnable r) {
            this.r = r;
        }

        @Override
        public void run() {
            while(true){
                r.run();

                synchronized (l){
                    l.add(this);
                }

                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void setRunnable(Runnable r){
            this.r = r;
        }

    }

    public void newjob(Runnable r){
        PooledThread myt;
        synchronized (l){
           if(l.isEmpty()){
               myt = new PooledThread(r);
               myt.start();
           }
           else {
               myt = l.remove(0);
               myt.setRunnable(r);
               myt.notify();
           }
        }

    }
}
