package Esami.esame280518;

public class HannaBarbera {

    public interface Food {
        int getWeight();
    }

    public interface Animal extends Food {
        void eat(Food f);
    }

    static abstract class Myanimal implements Animal{
        private int weight;

        public Myanimal(int weight) { this.weight = weight; }

        public abstract void eat(Food f);

        public int getWeight() { return weight; }
    }

    public static class Mouse extends Myanimal implements Animal {

        public Mouse(int weight) {
            super(weight);
        }

        @Override
        public void eat(Food f) { super.weight += f.getWeight() / 2; }

    }

    public static class Cat extends Myanimal implements Animal {

        public Cat(int weight) {
            super(weight);
        }

        @Override
        public void eat(Food f) { super.weight += f.getWeight() / 5; }

    }

    public static class Cheese implements Food {
        @Override
        public int getWeight() { return 50; }
    }

    public static void main(String[] args) {
        Animal tom = new Cat(200);
        Animal jerry = new Mouse(10);  //35
        //int c = tom.weight;
        jerry.eat(new Cheese());            //35
        jerry.eat(new Food() {              //40
            @Override
            public int getWeight() { return 10; }
        });
        tom.eat(jerry);                     //208
        System.out.println(String.format("Tom now weights %d", tom.getWeight()));
    }
}
