package Esami.esame200618;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;

public interface Function<A,B>{
    public B apply(A val);
}

class FuncyArrayList<E> extends ArrayList<E>{

    private static class FancyIterator<E>{
        private Function<E,E> f;
        private ArrayList<E> l;


        public FancyIterator(Function<E, E> f, ArrayList<E> l) {
            this.f = f;
            this.l = l;
        }

        public Iterator<E> iterator(int val){
            if(val >= 0)
                return new Iterator<E>() {
                    private int now = 0;

                    @Override
                    public boolean hasNext() {
                        return now < FancyIterator.this.l.size();
                    }

                    @Override
                    public E next() {
                        now++;
                        return f.apply(FancyIterator.this.l.get(now));
                    }
                };
            else{
                return new Iterator<E>() {
                    private int now = FancyIterator.this.l.size();

                    @Override
                    public boolean hasNext() {
                        return now > 0;
                    }

                    @Override
                    public E next() {
                        now--;
                        return f.apply(FancyIterator.this.l.get(now));
                    }
                };
            }
        }
    }


    Iterator<E> iterator(int step, Function<E, E> f){
        FancyIterator<E> interator = new FancyIterator<>(f, this);

        return interator.iterator(step);

    }

    public Iterator<E> iterator(){
        return this.iterator(1, (x)->x);
    }

    Iterator<E> backwardIterator(){
        return this.iterator(-1, (x)->x);
    }
}
