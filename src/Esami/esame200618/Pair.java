package Esami.esame200618;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Pair<A,B> {
    private A a;
    private B b;

    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    private static <E> Pair<E,E> mymethod(List<E> l){
        if(l.size()<2)
            return null;
        return new Pair<>(l.get(0), l.get(l.size()-1));
    }

    static <E> Pair<E, E> findMinAndMax(List<E> l, Comparator<E> c){
        Collections.sort(l,c);
        return mymethod(l);
    }

    static <E extends Comparable<E>> Pair<E, E> findMinAndMax(List<E> l){
        Collections.sort(l);

        return mymethod(l);
    }
}


