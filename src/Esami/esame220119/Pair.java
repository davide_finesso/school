package Esami.esame220119;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Pair<P, S> {
    public P primo;
    public S secondo;

    public Pair(P p, S s){
        this.primo = p;
        this.secondo = s;
    }
    @Override
    public String toString(){
        return this.primo.toString() + this.secondo.toString();
    }

    public static <A> Iterator<Pair<A, A>> pairIterator(Iterator<A> it){
        return new Iterator() {
            private A actual;
            @Override
            public boolean hasNext() {
                if(it.hasNext())
                    actual = it.next();
                return it.hasNext();
            }

            @Override
            public Pair<A, A> next() {
                return new Pair<>(actual, it.next());
            }
        };
    }
    public static <A,B> Iterator<B> mapIterator(Iterator<A> it, Main.myFunction<A,B> f) {
        return new Iterator<B>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public B next() {
                return f.apply(it.next());
            }
        };
    }

    public static Double pitagora(Pair<Integer, Integer> p){
        return Math.sqrt(Math.pow(p.primo,2)+Math.pow(p.secondo,2));
    }


}
