package Esami.esame220119;

import java.util.*;

import static Esami.esame220119.Pair.pairIterator;


public class Main {
    interface myFunction<A ,B>{
        B apply(A a);
    }

    public static <A,B> Iterator<B> mapIterator(Iterator<A> it, myFunction<A, B> f){
        return new Iterator<B>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public B next() {
                return f.apply(it.next());
            }
        };

        /*List<B> l = new ArrayList<>();

        while(it.hasNext()){
            l.add(f.apply(it.next()));
        }

        return l.iterator();*/

    }

    public static void main(String[] a){
        List<String> l = new ArrayList();


        Iterator<Integer> it = mapIterator(l.iterator(), (x)->x.length() );
        Iterator<Integer> it2 = mapIterator(l.iterator(), new myFunction<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return s.length();
            }
        });



    }


}
