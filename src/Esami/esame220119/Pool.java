package Esami.esame220119;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Function;

public interface Pool<T, R> {
    void add(T x); // popola la pool con un nuovo elemento
    R acquire() throws InterruptedException; // acquisisce una risorsa
    void release(R x); // rilascia una risorsa e la rimette nella pool
}

interface BasicPool<T> extends Pool<T,T>{}

class SimplePool<T> implements BasicPool<T>{
    LinkedBlockingQueue<T> l = new LinkedBlockingQueue<>();

    @Override
    public void add(T x) {
        l.add(x);
    }

    @Override
    public T acquire() throws InterruptedException {
        return l.take();
    }

    @Override
    public void release(T x) {
        add(x);
    }
}

class AutoPool<T> implements Pool<T,Resource<T>>{
    LinkedBlockingQueue<T> l = new LinkedBlockingQueue<>();

    @Override
    public void add(T x) {
        l.add(x);
    }

    @Override
    public Resource<T> acquire() throws InterruptedException {
        return new Resource<T>(l.take(), this);
    }

    @Override
    public void release(Resource<T> x) {
        l.add(x.elem);
    }
}


class Resource<T>{
    private AutoPool a;
    T elem;

    public Resource(T elem, AutoPool l){
        this.elem=elem;
        this.a = l;
    }

    public void release(){
        a.release(this);
    }

    @Override
    protected void finalize() throws Throwable {
        this.release();
    }
}
