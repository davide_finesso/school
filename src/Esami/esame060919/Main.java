package Esami.esame060919;

import java.util.*;

public class Main {

    static <T extends Polygon> T max(Collection<T> c , Comparator<? super T> comp){
        List<T> l = new ArrayList<>();
        for (T e: c ) {
            l.add(e);
        }

        Collections.sort(l, comp);

        return l.get(0);

    }

    public static void main(String[] ar){
        Rectangle.Square sq1 = new Rectangle.Square(new Point(10., -4.), 0.1),
                sq2 = new Rectangle.Square(new Point(1., 20.), 0.01);
        Collection<Rectangle.Square> squares = List.of(sq1, sq2);
        Rectangle r = max(squares, new Comparator<Polygon>() {
            @Override
            public int compare(Polygon a, Polygon b) {
                return (int) (b.area() - a.area());
            }
        });

        System.out.println(r.area());
    }
}
