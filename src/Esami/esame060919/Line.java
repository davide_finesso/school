package Esami.esame060919;

public class Line {
    protected Point p1;
    protected Point p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public double lenght(){
        return Math.sqrt(Math.pow(Math.abs(p1.x- p2.x)+Math.abs(p1.y-p2.y), 2));
    }
}
