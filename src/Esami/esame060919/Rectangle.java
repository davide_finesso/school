package Esami.esame060919;

import java.awt.*;
import java.util.List;

public class Rectangle extends Polygon {

    public Rectangle(Point p1, Point p3) {
        super(List.of(p1, new Point(p1.x,p3.y), p3, new Point(p3.x,p1.y)));

    }
    @Override
    public double area() {
        Line base = new Line(this.points.get(0), this.points.get(1));
        Line altezza = new Line(this.points.get(1), this.points.get(2));

        return base.lenght()* altezza.lenght();
    }


    public static class Square extends Rectangle {
        private double side;

        public Square(Point p1, double side) {
            super(p1, new Point(p1.x+side, p1.y+side));
            this.side = side;
        }
    }

}
