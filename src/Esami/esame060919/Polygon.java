package Esami.esame060919;

import java.util.Iterator;
import java.util.List;

public abstract class Polygon {
    protected final List<Point> points;

    protected Polygon(List<Point> points) {
        assert points.size() >= 3;
        this.points = points;
    }

    public Iterator<Line> lineIterator(){ return new Iterator<Line>() {
        private int pos = 0;
        private Point start;

        @Override
        public boolean hasNext() {
            if(pos == 0)
                start = points.get(pos);
            pos++;
            return pos <= points.size();
        }

        @Override
        public Line next() {
            Line l;
            if(pos != points.size())
                l = new Line(points.get(pos-1), points.get(pos));
            else
                l = new Line(points.get(pos), start);
            pos++;
            return l;
        }
    };}
    public double perimeter() {
        double somma = 0;

        Iterator<Line> it = this.lineIterator();

        while(it.hasNext()){
            somma += it.next().lenght();
        }
        return somma;
    }
    public abstract double area();
}
