package Esami.esame060919;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Triangle extends Polygon {

    public Triangle(Point p1, Point p2, Point p3) {
        super(List.of(p1,p2,p3));
    }

    @Override
    public double area() {
        Line ab = new Line(this.points.get(0), this.points.get(1));
        Point h = new Point( this.points.get(0).x, this.points.get(2).y);
        Line ah = new Line(this.points.get(0), h);
        return Math.abs(ab.lenght()* ah.lenght()/2);
    }
}
