package Esami.esame060919;

public class TriangleRectagle extends Triangle{

    public TriangleRectagle(Point p1, Point p2) {
        super(p1, p2, new Point(p1.x,p2.y));
        assert p1.x == p2.x || p1.y == p2.y;
    }
}
