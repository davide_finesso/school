package Esami.esame200619;

import java.net.CookieHandler;
import java.util.*;

public class Main {

    static <T> T max(Collection<T> c, Comparator<? super T> cmp){
        List<T> l = new ArrayList<>();
        for (T e: c ) {
            l.add(e);
        }
        Collections.sort(l,cmp);

        return l.get(0);
    }

    public static void main(String[] a){
        Person alice = new Person("Alice", 23),
                david = new Artist("Bowie", 69, new Hair(75, Set.of(Color.RED, Color.BROWN, Color.GRAY)));
        Artist morgan = new Artist("Morgan", 47, new Hair(20, Set.of(Color.GRAY, Color.DARK))),
                madonna = new Artist("Madonna", 60, new Hair(50, Set.of(Color.BLONDE)));


        List<Artist> artists = Arrays.asList((Artist) david, morgan, madonna);
        List<Person> persons = Arrays.asList(alice, david, morgan, madonna);

        Artist lambartist = max(artists, (x,y)-> x.hair.length*x.hair.colors.size() - y.hair.length*y.hair.colors.size());

        Artist myartist = max(artists, new Comparator<Artist>() {
            @Override
            public int compare(Artist x, Artist y) {
                return x.hair.length*x.hair.colors.size() - y.hair.length*y.hair.colors.size();
            }
        });

        Person mylambaperson = max(persons, (x,y)-> x.name.compareTo(y.name));

        Person myperson = max(persons, new Comparator<Person>() {
            @Override
            public int compare(Person x, Person y) {
                return x.name.compareTo(y.name);
            }
        });


        Artist c = Collections.max(artists, new Comparator<Person>() {
            public int compare(Person a, Person b) {
                return a.age - b.age;
            }
        });




    }
}
