package Esami.esame200619;

interface Equatable<T> {
    boolean equalsTo(T x);
}
