package Esami.esame200619;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class myMain {
    public static void main(String[] arg){

        Node<Integer> uno = new Node(1,null, null);
        Node<Integer> tre = new Node<>(3,null,null);
        Node<Integer> root = new Node<>(2, tre,uno);

        Iterator<Node> it = uno.iterator();

        while(it.hasNext()){
            System.out.println(it.next().elem);
        }
    }
}
