package Esami.esame200619;

public class Person<P extends Person<P>> implements Equatable<P> {
    public final String name;
    public final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Person){
            P per = (P) o;
            return this.equalsTo(per);
        }
        return false;
    }

    @Override
    public boolean equalsTo(P other) {
        return this.name == other.name && this.age == other.age;
    }

    @Override
    public String toString() { return name; }

}
