package Esami.esame200619;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Node<T> {
    //private static List<Node> nodelist = new ArrayList<>(50);
    public T elem;
    private Node dx;
    private Node sx;

    public Node(T elem, Node dx, Node sx) {
        this.elem = elem;
        this.dx = dx;
        this.sx = sx;

    }

    private static void myfun(List<Node> l, Node e){
        if(e != null){
            myfun(l, e.sx);
            l.add(e);
            myfun(l, e.dx);
        }

    }

    public Iterator<Node> iterator() {
        List<Node> l = new ArrayList();

        myfun(l, this );

        return l.iterator();

    }
}

