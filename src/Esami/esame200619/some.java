package Esami.esame200619;

import java.util.Set;

class Artist extends Person<Artist> {
    public final Hair hair;
    public Artist(String name, int age, Hair hair) {
        super(name, age);
        this.hair = hair;
    }

    @Override
    public boolean equalsTo(Artist other) {
        return this.hair.equalsTo(other.hair) && super.equalsTo(other);
    }
}
class Hair implements Equatable<Hair> {
    public final int length;
    public final Set<Color> colors;
    public Hair(int length, Set<Color> colors) {
        this.colors = colors;
        this.length = length;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof Hair) {
            Hair h = (Hair) o;
            return this.equalsTo(h);
        }
        return false;
    }
    @Override
    public boolean equalsTo(Hair x) {
        for (Color c : this.colors ) {
            if(!x.colors.contains(c))
                return false;
        }
        return true;
    }
}
enum Color {
    BROWN, DARK, BLONDE, RED, GRAY;
}
