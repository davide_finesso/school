package Esami.esame310620;

import java.util.Iterator;

public class Sphere implements Solid {
    private double ray;

    public Sphere(double ray) {
        this.ray = ray;
    }

    @Override
    public double area() {
        return Math.pow(ray, 2)* 4* 3.14;
    }

    @Override
    public double volume() {
        return (4/3) * 3.14* Math.pow(ray,3);
    }

    @Override
    public PositionedSolid at(Point origin) {
        return () -> new Point(0,0,0);
    }

}
