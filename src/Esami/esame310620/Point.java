package Esami.esame310620;

public class Point {
    protected final double x;
    protected final double y;
    protected final double z;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point move(double dx, double dy, double dz){
        return new Point(x+dx, y+dy,z+dz);
    }
}
