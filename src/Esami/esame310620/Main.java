package Esami.esame310620;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Cube cube1 = new Cube(11.), cube2 = new Cube(23.);
        Sphere sphere1 = new Sphere(12.), sphere2 = new Sphere(35.);
        List<Solid> solids = List.of(cube1, cube2, sphere1, sphere2);
        List<Cube> cubes = List.of(cube1, cube2);
        List<Sphere> spheres = List.of(sphere1, sphere2);
        List<? extends Polyhedron> polys = cubes;


        /*Collections.sort(cubes);

        Collections.sort(spheres, Solid.comparatorBy(Sphere::area));

        Comparator<Cube> cmpCube = Solid.comparatorBy(Cube::perimeter);
        Comparator<Solid> cmpSolid = Solid.comparatorBy(Solid::area);
        Comparator<Polyhedron> cmpPoly2 = Solid.comparatorBy(Solid::volume);
        Comparator<Cube> cmpCube2 = Solid.comparatorBy(Polyhedron::perimeter);

        Comparator<Polyhedron> cmpPoly = Solid.comparatorBy(Polyhedron::volume);
        //Comparator<Sphere> cmpSphere = Solid.comparatorBy(Sphere::perimeter);
        Comparator<Sphere> cmpSphere2 = Solid.comparatorBy(Solid::area);
        Comparator<Cube> cmpCube3 = Solid.comparatorBy(Polyhedron::perimeter);
        Collections.sort(solids, cmpSolid);

        Collections.sort(solids, cmpSolid);
        Collections.sort(cubes, cmpSolid);
        Collections.sort(cubes, cmpPoly);
        Collections.sort(spheres, cmpPoly2);
        Collections.sort(polys, cmpSolid);

        Iterator it = polys.iterator();

        //Polyhedron p = new Cube(5);

        while (it.hasNext()){
            Polyhedron o =  it.next();
//        }
*/
        for (Polyhedron p: polys) {
            PositionedPolyhedron pp = p.at(new Point(1,1,1));

            Iterator<Point> it = pp.iterator();

            while(it.hasNext()){
                Point point = it.next();
                System.out.println("x="+ point.x+ ", y="+ point.y+", z="+ point.z);
            }
        }


    }
}
