package Esami.esame310620;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class Cube implements Polyhedron {
    private double side;

    public Cube(double side) {
        this.side = side;
    }

    @Override
    public double area() {
        return Math.pow(side, 2) * 6;
    }

    @Override
    public double volume() {
        return Math.pow(side, 3);
    }

    @Override
    public double perimeter() {
        return side * 12;
    }

    public Comparable comp(Comparable<Cube> c){
        return c;
    }


    @Override
    public PositionedPolyhedron at(Point origin) {
        return new PositionedPolyhedron() {
            Point p = new Point(origin.x, origin.y, origin.z);
            @Override
            public Point origin() {
                return new Point(p.x, p.y, p.z);
            }

            @Override
            public Iterator<Point> iterator() {
                List<Point> l = new ArrayList<>();
                Point p = origin();

                l.add(p);
                l.add(p.move(p.x+side, p.y, p.z));
                l.add(p.move(p.x, p.y+side, p.z));
                l.add(p.move(p.x+side, p.y+side, p.z));

                l.add(p.move(p.x, p.y, p.z+side));
                l.add(p.move(p.x+side, p.y, p.z+side));
                l.add(p.move(p.x, p.y+side, p.z+side));
                l.add(p.move(p.x+side, p.y+side, p.z+side));

                return l.iterator();
            }
        };
    }
}
