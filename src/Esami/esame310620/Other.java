package Esami.esame310620;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;


interface Solid extends Comparable<Solid> {
    double area();

    double volume();

    PositionedSolid at(Point origin);
    static <S extends Solid> int compareBy(Function<S, Double> f, S s1, S s2) {
        return Double.compare(f.apply(s1), f.apply(s2));
    }
    static <S extends Solid> Comparator<S> comparatorBy(Function<S,  Double> f) {
        return (s1, s2) -> compareBy(f, s1, s2);
    }
    default int compareTo(Solid s) {
        return compareBy(Solid::volume, this, s);
    }
}
interface Polyhedron extends Solid {
    double perimeter();
    @Override
    PositionedPolyhedron at(Point origin);

}
interface PositionedSolid {
    Point origin();
}
interface PositionedPolyhedron extends PositionedSolid, Iterable<Point> {}


