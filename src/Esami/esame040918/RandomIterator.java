package Esami.esame040918;

import java.util.Iterator;
import java.util.Random;

public class RandomIterator implements Iterator<Integer> {
    private final Random r;
    private final int val;
    private int now;

    public RandomIterator(int val) {
        this.val = val;
        this.now = 0;
        this.r = new Random();
    }

    @Override
    public boolean hasNext() {
        return now<=val;
    }

    @Override
    public Integer next() {
        now++;
        return r.nextInt();
    }
}
