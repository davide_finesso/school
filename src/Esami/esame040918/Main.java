package Esami.esame040918;

import java.util.Collection;
import java.util.Iterator;

public class Main {

    /*
    A -> cani estende Comparable ---- qualsiasi cosa sopra a gatti
    B -> gatti

     */

    public static <A extends Comparable<? super B>,B > int compareMany(Collection<A> a, Collection<B> b){
        int somma = 0;
        if(a.size()!=b.size())
            return a.size()-b.size();

        Iterator<A> ita = a.iterator();
        Iterator<B> itb = b.iterator();

        while(ita.hasNext())
            somma += ita.next().compareTo(itb.next());

        return somma;

    }
}
