package Esami.esame040918;

import java.util.Random;

public class SingletonRandom {
    private final Random r;
    private static SingletonRandom instance;

    private SingletonRandom(){
        this.r = new Random();
    }

    public Random getR(){
        return this.r;
    }

    public static SingletonRandom getRandom(Random rand){
        if(instance == null || !instance.r.equals(rand)){
            instance = new SingletonRandom();
        }
        return instance;
    }
}
