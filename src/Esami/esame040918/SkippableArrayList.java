package Esami.esame040918;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.function.Function;

public class SkippableArrayList<B> extends ArrayList<B> {

    public static void main(String[] arg){
        ArrayList<Integer> a = new ArrayList<>();
        SkippableArrayList<Integer> s = new SkippableArrayList<>();
        ArrayList<Integer> l = new ArrayList<>();

        Random r = new Random();

        for(int i = 0 ; i<10;i++){
            s.add(r.nextInt(10));
        }
        System.out.println("prima");
        for(Integer i : s){
            System.out.println(i);
        }

        Iterator<Integer> it = s.iterator((x) -> x > 5, new Either<Integer>() {
            @Override
            public Integer onSuccess(Integer elem) {
                return elem+1;
            }

            @Override
            public void onFailure(Integer elem) throws Exception {
                a.add(0, elem);
            }
        });

        while(it.hasNext())
            l.add(it.next());

        System.out.println("collection a");
        for(Integer i : a){
            System.out.println(i);
        }
        System.out.println("dopo incremento");
        for(Integer i : l){
            System.out.println(i);
        }
    }

    Iterator<B> iterator(Predicate<B> p, Either<B> f){
        return new Iterator<B>() {
            private int i = 0;

            @Override
            public boolean hasNext() {
                return i < SkippableArrayList.this.size();
            }
            @Override
            public B next() {
                if(p.apply(SkippableArrayList.this.get(i)))
                    return f.onSuccess(SkippableArrayList.this.get(i++));
                else {
                    try {
                        f.onFailure(SkippableArrayList.this.get(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return SkippableArrayList.this.get(i++);
                }

            }
        };
    }
}
interface Predicate<B> extends Function<B, Boolean>{

}

interface Either<T>{
    public T onSuccess(T elem);
    public void onFailure(T elem) throws Exception;
}