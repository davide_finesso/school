package Wildcard;

import tinyjdk.Myclass;
import tinyjdk.Myclasssecond;

import java.util.*;

public class Sorting {

    static <T> boolean addAll(Collection<? super T> c, T... elements){
        return false;
    }

    static <T> void sort(List<T> l , Comparator<? super T> c){
        Collections.sort(l, c);
    }

    static <T extends Comparable<? super T>> void sort(List<T> l){
        Collections.sort(l);
    }




    static public void main(String[] args){
        List<Myclass> l = new ArrayList();

        Myclasssecond a = new Myclasssecond(1);
        Myclasssecond b = new Myclasssecond(4);

        l.add(new Myclasssecond(3));
        l.add(new Myclasssecond(2));

        Collections.addAll(l, a, b);

        //Myclasssecond mk = l.get(l.indexOf(a));

        //int f = mk.cucu;

        //Collections.sort(l, );

        Collections.sort(l,(o1, o2 )-> o1.a - o2.a );
        /*   TRASFORMAZIONE LAMBDA
        Sorting.sort(l, new Comparator<Myclass>() {
            @Override
            public int compare(Myclass o1, Myclass o2) {
                return o1.a-o2.a;
            }
        });

        Sorting.sort(l);


         */

        while(!l.isEmpty()) {
            System.out.println(l.remove(0).a);
        }
    }
}
