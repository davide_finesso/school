package tinyjdk;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Test {

    public static int f(int n, Function<Integer, Integer> g) {
        return g.apply(n);
    }
    public static void f(int n, Consumer<Integer> g) {
        g.accept(n);
    }
    public static void f(int n, Runnable g) {
        g.run();
    }
    public static int f(int n, Supplier<Integer> g) {
        return g.get();
    }

    public static <A, B> List<B> map(Iterable<A> c, Function<A, B> f){          //POLIMORFISMO FIRST CLASS (GENERICS LOCALI?)
        List<B> l = new ArrayList<>();

        for (A x: c )
            l.add(f.apply(x));

        return l;
    }

    public static void main(String[] args){
        ArrayList<Myclasscomp> arr = new ArrayList<>();
        List<Myclass> l = new ArrayList<>();
        List<Myclasssecond> arr1 = new ArrayList<>();
        Comparator c = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Myclass && o2 instanceof Myclass){
                    Myclass a = (Myclass) o1;
                    Myclass b = (Myclass) o2;
                    return a.a -b.a;
                }
                return 0;
            }
        };

        Collections.sort(arr);

        Collections.sort(arr1, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Myclass && o2 instanceof Myclass){
                    Myclass a = (Myclass) o1;
                    Myclass b = (Myclass) o2;
                    return a.b -b.a;
                }
                return 0;
            }
        });

        Collections.sort(arr1, (o1, o2)-> o1.a-o2.a);
        
        //VOLGIO TRASFORMARE L DA UNA LISTA DI MYCLASS IN UNA LISTA DI INTEGER
        
        Function<Myclass, Integer> f = new Function<Myclass, Integer>() {
            @Override
            public Integer apply(Myclass myclass) {
                return myclass.a;
            }
        };
        
        List<Integer> l1 = map(l, (Myclass myclass) -> myclass.a );
        

    }

}

    //public static <T> void sort(List<T> list, Comparator<? super T> c)
    //public static <T extends Comparable<T>> void sort(List<T> list){};
