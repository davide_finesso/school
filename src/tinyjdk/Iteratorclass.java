package tinyjdk;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class Iteratorclass {

    public static <A> Iterator<A> reverseinterator(Iterator<A> it){
        List<A> l =  new ArrayList();
        while(it.hasNext())
            l.add(it.next());

        return new Iterator<A>() {
            private int pos = l.size()-1;

            @Override
            public boolean hasNext() {
                return pos>=0;
            }

            @Override
            public A next() {
                return l.get(pos--);
            }
        };
    }

    public static <A> Iterator<A> reverseinterator1(Iterator<A> it){
        List<A> l = new ArrayList<>();
        while(it.hasNext())
            l.add(0, it.next());

        return l.iterator();
    }

    public static class ArrRevit<T> extends ArrayList<T>{

        @Override
        public Iterator<T> iterator() {
            return new Iterator<T>() {
                private int pos = ArrRevit.this.size() -1;

                @Override
                public boolean hasNext() {
                    return pos>=0;
                }

                @Override
                public T next() {
                    return ArrRevit.this.get(pos);
                }
            };
        }

    }

    public static class StepArrayList<T> extends ArrayList<T>{
        public final int step;

        public StepArrayList(int step) {
            this.step = step;
        }

        @Override
        public Iterator<T> iterator() {
            return new Iterator<T>() {
                private int pos = 0;

                @Override
                public boolean hasNext() {
                    return pos < StepArrayList.this.size();
                }

                @Override
                public T next() {
                    T t = StepArrayList.this.get(pos);
                    pos += step;

                    return t;
                }
            };
        }

        public static <A, B> Iterator<B> mapIterator(Iterator<A> it, Function<? super A, ? extends B> f) {
            /*return new Iterator<B>() {
                private int pos = 0;

                @Override
                public boolean hasNext() {
                    return it.hasNext();
                }

                @Override
                public B next() {
                    return f.apply(it.next());
                }
            };*/

            List<B> l = new ArrayList();

            while (it.hasNext())
                l.add(f.apply(it.next()));

            return l.iterator();
        }

        public static <A, B> Iterator<B> mapAndRevIterator(Iterator<A> it, Function<A, B> f) {
            return mapIterator(reverseinterator1(it), f);
        }
    }
}
