package tinyjdk;

public class Myclass implements Comparable{
    public int a;
    public int b;
    public String d;

    public Myclass(){
        a=0;
        b=0;
        d="ccc";
    }
    public Myclass(int a){
        this.a =a;
    }


    @Override
    public int compareTo(Object o) {
        if( o instanceof Myclass) {
            Myclass o1 = (Myclass) o;
            return this.a - o1.a;
        }
        return 0;
    }

    public Myclass myMethod(Myclasssecond m){
        return new Myclass();
    }
}
