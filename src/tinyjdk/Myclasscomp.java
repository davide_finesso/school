package tinyjdk;

import java.util.ArrayList;

public class Myclasscomp implements Comparable {
    int a;
    int b;
    String d;


    @Override
    public int compareTo(Object o) {
        if(o instanceof Myclasscomp) {
            Myclasscomp o1 = (Myclasscomp) o;
            return this.a - o1.a + this.b - o1.b;
        }
        return 0;
    }
}
